var bcrypt = require('bcrypt');

function User(sql, config, email, id, username){
    this.id = id;
    this._email = email;
    this._sql = sql;
    this._config = config;
    this._username = username;
    this._connected = false;
}
User.prototype.verifyPassword = function(password){
    return new Promise((res, rej) =>{
        this.connect().then(() => {
            return this._pool.request().query("SELECT password FROM [user] WHERE email='" + this._email + "';");
        }).then(response => {
            if(response && response.recordset && response.recordset[0] && response.recordset[0].password){
                return(bcrypt.compare(password, response.recordset[0].password));
            }else{
                rej('no password was provided');
            }
        }).then(response => {
            if(typeof(response) === typeof(true)){
                res(response);
            }else{
                rej("Failed to compare with bcrypt");
            }
        }).catch(err => {
            rej(err);
        });
    });
}
User.prototype.connect = function(){
    return new Promise((res, rej) => {
        if(this._connected === true){
            res();
        }else{
            if(!this._pool){
                this._pool = new this._sql.ConnectionPool(this._config);
            }
            this._pool.connect().then(() => {
                this._connected = true;
                res();
            }).catch(function(err){
                rej(err);
            });
        }
    });
}
module.exports = User;