module.exports = function(app, db, passport){
    app.get('/', function(req, res, next){
        if(req.isAuthenticated()){
            res.status(200).render('index_postauth');
        }else{
            res.status(200).render('index_preauth', {layout: 'preauth'});
        }
    });
    app.get('/login', function(req, res, next){
        if(req.isAuthenticated()){
            res.redirect('/');
        }else{
            res.status(200).render('login', {layout: 'preauth', message: req.flash('loginMessage')});
        }
    });
    app.post('/login', passport.authenticate('local-login', {
                    successRedirect: '/', 
                    failureRedirect: '/login',
                    failureFlash: true
            })
    );
    app.get('/profile', loggedIn, function(req, res, next){
        res.status(200).render('profile', {user: req.user});
    });
    app.get('/signup', function(req, res, next){
        if(req.isAuthenticated()){
            res.redirect('/');
        }else{
            res.status(200).render('signup', {layout: 'preauth', message: req.flash('loginMessage')});
        }
    });
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/profile',
        failureRedirect: '/signup',
        falureFlash: true
    }));
    app.get('/logout', function(req, res){
        req.logout();
        res.redirect('/');
    });
}
function loggedIn(req, res, next){
    if(req.isAuthenticated()){
        next();
    }
    else{
        res.redirect('/login')
    }
}