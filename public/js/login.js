function showAttemptText(){
    console.log('showing attempt text...');
    document.getElementById('login-attempt-overlay').classList.remove('hidden');
}
window.addEventListener('DOMContentLoaded', function () {
    var loginRedirButton = document.getElementById('submit-login-button');
    if (loginRedirButton) {
      loginRedirButton.addEventListener('click', showAttemptText);
  }
});