'use strict';
var Promise = require('bluebird');
var express = Promise.promisifyAll(require('express'));
var handlebars = require('handlebars');
var path = require('path');
var exphbs = require('express-handlebars');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var validator = require('express-validator');//TODO: USE THIS
var passport = Promise.promisifyAll(require('passport'));
var flash = require('express-flash');
var port = process.env.PORT || 8080;
var db = require('./database.js');
var app = express();
//set up passport
require('./passport.js')(passport, db);

//set up handlebars 
var hbs = exphbs.create({
    helpers: {
        checkMinLength: function(v1, v2, options){
            if(v1.length>=v2){
                return options.fn(this);
            }
            return options.inverse(this);
        }
    }, 
    defaultLayout: 'postauth', 
    extname: '.hbs'
})
//set up express
app.engine('hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(logger('dev'));
app.use(cookieParser(process.env.SESSION_SECRET));
app.use(bodyParser.urlencoded({ extended : false}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret:process.env.SESSION_SECRET,resave:true,saveUninitialized:true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//set up express routes
require('./routes.js')(app, db, passport);
//begin listening for requests
app.listen(port, () => {
    console.log('listening on port ' + port + '.');
});