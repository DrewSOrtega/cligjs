'use strict';
var Promise = require('bluebird');
var bcrypt = require('bcrypt');
var sql = Promise.promisifyAll(require('mssql'));
const config = require('./config.js');
var User = require('./user.js');
function Database(){
    this._connected = false;
}
Database.prototype.connect = function(){
    return new Promise((res, rej) => {
        if(this._connected === true){
            res();
        }else{
            if(!this._pool){
                this._pool = new sql.ConnectionPool(config);
            }
            this._pool.connect().then(() => {
                this._connected = true;
                res();
            }).catch(function(err){
                rej(err);
            });
        }
    });
}
Database.prototype.addUser = function(email, username, password, first_name, last_name, birthdate, country){
    return new Promise((res, rej) =>{
        var pass_hash = "";
        bcrypt.hash(password, 10).then(hash => {
           pass_hash = hash;
           return this.connect();
        }).then(() =>{
            //have addUser use inpuuted country and bday instead of hardcoded
            var q = "INSERT INTO [user] (email, username, password, first_name, last_name, birthdate, country) VALUES  ("
            + "'" + email+ "'" + ", "
            + "'" + username + "'" + ", "
            + "'" + pass_hash +  "'" + ", "
            + "'" + first_name + "'" + ", "
            + "'" + last_name + "'" + ", "
            + "'" + "1999-04-22" + "'" + ", "
            + "'" + "US"+ "'" + ");";
            console.log(q);
            return this._pool.request().query(q);
        }).then(response => {
            res();
        }).catch(err => {
            rej(err);
        });
    });
}
Database.prototype.getUsers = function(email){
    
    this.connect().then(() => {
        return this._pool.request().query("SELECT id, first_name, email FROM [user];");
    }).then(response => {
        console.log(response);
    }).catch(err => {
        console.log(err);
    });
}   
Database.prototype.disconnect = function(){
    if(this._pool){
        this._pool.close();
    }
    _connected = false;
}
Database.prototype.isConnected = function(){
    return this._connected;
}
Database.prototype.getUser = function(email){
    return new Promise((res, rej) => {
        this.getIdFromEmail(email).then(id => {
            if(typeof(id) === typeof(1)){
                return this.findById(id);
            }else{
                rej('did not get proper response when checking user exist');
            }
        }).then(user => {
            res(user);
        }).catch(err => {
            rej(err);
        });
    });
}
Database.prototype.findById = function(id){
    return new Promise((res, rej) => {
        this.checkUserExists(id).then(response => {
            if(response === false){
                res();
            }else if (response == true){
                return this._pool.request().query("SELECT email, username FROM [user] WHERE id='" + id + "';");
            }else{
                rej('did not get proper response when checking user exist');
            }
        }).then(response => {
            res(new User(sql, config, response.recordset[0].email, id, response.recordset[0].username));
        }).catch(err => {
            rej(err);
        });
    });
}
Database.prototype.getIdFromEmail = function(email){
    return new Promise((res, rej) => {
        this.connect().then(() => {
            return this._pool.request().query("SELECT id FROM [user] WHERE email='" + email + "';");
        }).then(response => {
            if(response && response.recordset && response.recordset[0] && typeof(response.recordset[0].id) === typeof(1)){
                res(response.recordset[0].id)
            }else{
                rej("got unfull query getting id from email");
            }
        }).catch(err => {
            rej(err);
        })
    });
}
Database.prototype.checkUserExists = function(id){
    return new Promise((res, rej) => {
        this.connect().then(() => {
            return this._pool.request().query("SELECT COUNT(*) FROM [user] WHERE id=" + id + ";");
        }).then(response => {
            if(response && response.recordset && response.recordset[0] && typeof(response.recordset[0]['']) === typeof(1)){
                if(response.recordset[0][''] === 1){
                    res(true);
                }else if(response.recordset[0][''] < 1){
                    res(false);
                }else if(response.recordset[0][''] > 1){
                    rej("FATAL ERROR: Got multiple users with the same email!!!");
                }else{
                    rej("got invalid format from query");
                }
            }else{
                rej("got unfull query");
            }
        }).catch(err => {
            rej(err);
        })
    });
}
Database.prototype.alreadyRegistered = function(email, username){
    return new Promise((res, rej) => {
        this.connect().then(() => {
            return this._pool.request().query("SELECT COUNT(*) FROM [user] WHERE email='" + email + "' OR username='" + username + "';");
        }).then(response => {
            if(response && response.recordset && response.recordset[0] && typeof(response.recordset[0]['']) === typeof(1)){
                if(response.recordset[0][''] === 0){
                    res(true);
                }else if(response.recordset[0][''] < 0 || response.recordset[0][''] > 0){
                    res(false);
                }else{
                    rej("got invalid format from query");
                }
            }else{
                rej("got unfull query");
            }
        }).catch(err => {
            rej(err);
        })
    });
}
function createTransaction(pool){
    return new Promise(function(res, rej){
        try{
            var transaction = new sql.Transaction(pool);
            res(transaction);
        }catch{
            rej('fatal error creating transaction');
        }
    });
}
function accessDB(pool, request){
    let results = {};
    return new Promise(function(res, rej){
        createTransaction(pool).then(function(tx){
            results.tx = tx;
            console.log('sucesfully created transaction');
            return results.tx.begin();
        }).then(function(){ //callback of transaction begin
            console.log('succesfully began transaction');
            return request(results.tx, pool);
        }).then(function(){
            console.log('sucessfully ran request');
            return results.tx.commit();
        }).then(function(){
            console.log('succesfully committed request');
        }).catch(function(err){
            rej(err);
        });
    })
}
function test(){
    accessDB(function(tx, pool){
        return new Promise(function(res, rej){
            const request = new sql.Request(tx);
            request.query("SELECT * FROM [user] WHERE UserID=0;").then(function(result){
                console.log(result.recordset);
                res();
            }).catch(function(err){
                rej(err);
            });
        });
    });
}
module.exports = exports = new Database();