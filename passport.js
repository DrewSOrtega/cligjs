var LocalStrategy = require('passport-local').Strategy;
var Promise = require('bluebird');
module.exports = function(passport, db){
    passport.serializeUser(function(user, done) {
        console.log("serializeUser id: " + user.id);
        done(null, user.id);
    });
       
    passport.deserializeUser(function(id, done) {
        console.log("deserializeUser id: " + id);
        db.findById(id).then(user =>{
            if(user){
                done(null, user);
            }else{
                done("user with id " + id + " does not exist");
            }
        }).catch(err => {
            done(err);
        });
    });

    passport.use('local-login', new LocalStrategy(
        {
            usernameField: 'email',
            passwordfield: 'password',
            passReqToCallback : true
        },
        function(req, email, password, done) {
            db.getUser(email).then(user =>{
                if(user!=null && user.id){
                    this.user = user;
                    return user.verifyPassword(password);
                }
            }).then(response => {
                if(response === false){
                    console.log('incorrect password');
                    return done(null, false, req.flash('loginMessage', 'Incorrect Email/Password.'));
                }else if(response === true){
                    return done(null, this.user);
                }else{
                    console.log('incorrect username');
                    return done(null, false, req.flash('loginMessage', 'Incorrect Email/Password.'));
                }
            }).catch(err => {
                done(err);
            });
        }
    ));
    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'email',
            passwordfield: 'password',
            passReqToCallback : true
        },
        function(req, email, password, done) {
            db.alreadyRegistered(email, req.body.username).then(response =>{
                if(response === true){
                    //TODO: checks to see if form is filled out correctly!!!!
                    return db.addUser(email, req.body.username, password, req.body.first_name, req.body.last_name, req.body.birthdate, req.body.country);
                }else{
                    done(null, false, req.flash('signupMessage', 'Username or Email already Taken.'));
                }
            }).then(() => {
                return db.getUser(email);
            }).then(user => {
                return done(null, user);
            }).catch(err => {
                done(err);
            });
        }
    ));
}