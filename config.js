module.exports = config = {
    user: 'orvyx',
    password: process.env.SQL_PW,
    server: process.env.SQL_URL,
    database: 'clig',
    options: {
        encrypt: false
    }
}